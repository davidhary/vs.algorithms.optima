﻿Imports isr.Algorithms.Optima
Namespace OptimaTests

    '''<summary>
    '''This is a test class for RosenbrockFunctionSimplexTest and is intended
    '''to contain all RosenbrockFunctionSimplexTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class RosenbrockFunctionSimplexTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
                Solution.ResetRandomGenerator(RosenbrockFunctionSimplexTestInfo.Get.Seed)
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            Assert.IsTrue(RosenbrockFunctionSimplexTestInfo.Get.Exists, $"{GetType(RosenbrockFunctionSimplexTestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()


            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " SOLVER "

        '''<summary> A test for Solver. </summary>
        <TestMethod()>
        Public Sub RosenbrockFunctionMinimization()

            TestInfo.TraceMessage("Begin simplex method optimization demo")
            TestInfo.TraceMessage("Solving Rosenbrock function f(x,y) = 100*(y-x^2)^2 + (1-x)^2")
            TestInfo.TraceMessage("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0")

            Dim dimension As Integer = 2 ' problem dimension (number of variables to solve for)
            Dim minX As Double = RosenbrockFunctionSimplexTestInfo.Get.Minimum
            Dim maxX As Double = RosenbrockFunctionSimplexTestInfo.Get.Maximum
            Dim maxLoop As Integer = RosenbrockFunctionSimplexTestInfo.Get.IterationCount
            Dim objectivePrecision As Double = RosenbrockFunctionSimplexTestInfo.Get.Objective
            Dim valuesPrecision As Double = RosenbrockFunctionSimplexTestInfo.Get.Convergence

            ' an simplex method optimization solver
            Dim simplex As New Simplex("Rosenbrock", dimension, minX, maxX, maxLoop, valuesPrecision, objectivePrecision)
            simplex.Initialize(Solution.Random, New RosenbrockObjectiveFunction)

            TestInfo.TraceMessage("Initial simplex is: ")
            TestInfo.TraceMessage(simplex.ToString)

#If True Then
            Dim sln As Solution = simplex.Solve
#Else
        Dim simplexSize As Integer = dimension + 1 ' number of potential solutions in the simplex
        a.IterationNumber = 0
        Do Until a.HitCountOverflow OrElse a.Converged OrElse a.Optimized

            a.IterationNumber += 1
            If True Then
                If a.IterationNumber = 1 OrElse a.IterationNumber Mod 10 = 0 Then
                    Console.Write("At t = " & a.IterationNumber & " current best solution = ")
                    Console.WriteLine(a.BestSolution)
                End If
            Else
                Console.Write("At t = " & a.IterationNumber & " current best solution = ")
                Console.WriteLine(a.BestSolution)
            End If
            ' run one step.            
            a.SolveStep()
        Loop ' solve loop
#End If

            TestInfo.TraceMessage("Final simplex is: ")
            TestInfo.TraceMessage(simplex.ToString)

            TestInfo.TraceMessage("Best solution found after {0} iterations: ", simplex.IterationNumber)
            TestInfo.TraceMessage(sln.ToString)

            TestInfo.TraceMessage("End simplex method optimization test")

            Dim actual As Double = sln.Objective
            Dim expected As Double = 0
            Assert.AreEqual(expected, actual, objectivePrecision)

        End Sub
#End Region

    End Class
End Namespace