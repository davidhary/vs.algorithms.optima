﻿Imports isr.Core.Agnostic.RandomExtensions
Namespace OptimaTests
    ''' <summary> A pulse response simplex test. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="9/23/2017" by="David" revision=""> Created. </history>
    <TestClass()>
    Public Class PulseResponseSimplexTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
                Algorithms.Optima.Solution.ResetRandomGenerator(DateTime.Now.Millisecond)
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            Assert.IsTrue(PulseResponseSimplexTestInfo.Get.Exists, $"{GetType(PulseResponseSimplexTestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()

            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " BUILDERS "

        ''' <summary> Enumerates build time series in this collection. </summary>
        ''' <param name="sampleInterval"> The sample interval. </param>
        ''' <param name="count">          Number of elements. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process build time series in this collection.
        ''' </returns>
        Private Shared Function BuildTimeSeries(ByVal sampleInterval As Double, ByVal count As Integer) As IEnumerable(Of Double)
            Dim l As New List(Of Double)
            For i As Integer = 0 To count - 1
                l.Add(i * sampleInterval)
            Next
            Return l
        End Function

        ''' <summary> Returns an exponent. </summary>
        ''' <param name="asymptote">    The asymptote. </param>
        ''' <param name="timeConstant"> The time constant. </param>
        ''' <param name="timeSeries">   The time series. </param>
        ''' <returns> A list of time and amplitude values. </returns>
        Private Shared Function BuildExponent(ByVal asymptote As Double, ByVal timeConstant As Double,
                                          ByVal timeSeries As IEnumerable(Of Double)) As IEnumerable(Of System.Windows.Point)
            Dim l As New List(Of System.Windows.Point)
            Dim TinverseTau As Double = 0
            Dim t As Double = 0
            Dim value As Double = 0
            For i As Integer = 0 To timeSeries.Count - 1
                t = timeSeries(i)
                TinverseTau = t / timeConstant
                value = asymptote * (1 - Math.Exp(-TinverseTau))
                l.Add(New System.Windows.Point(t, value))
            Next
            Return l
        End Function

        ''' <summary> Enumerates add in this collection. </summary>
        ''' <param name="timeSeries"> The time series. </param>
        ''' <param name="noise">      The noise. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process add in this collection.
        ''' </returns>
        Private Shared Function Add(ByVal timeSeries As IEnumerable(Of System.Windows.Point),
                                ByVal noise As IEnumerable(Of Double)) As IEnumerable(Of System.Windows.Point)
            Dim l As New List(Of System.Windows.Point)
            For i As Integer = 0 To timeSeries.Count - 1
                l.Add(New System.Windows.Point(timeSeries(i).X, timeSeries(i).Y + noise(i)))
            Next
            Return l
        End Function

        ''' <summary> Builds an exponent using integration. </summary>
        ''' <param name="asymptote">    The asymptote. </param>
        ''' <param name="timeConstant"> The time constant. </param>
        ''' <param name="count">        Number of. </param>
        ''' <returns> A list of. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Private Shared Function Integrate(ByVal asymptote As Double, ByVal timeConstant As Double, ByVal count As Integer) As IEnumerable(Of System.Windows.Point)
            Dim l As New List(Of Windows.Point)
            Dim vc As Double = 0
            Dim deltaT As Double = 0.0001
            Dim deltaV As Double = deltaT * (asymptote - vc) / timeConstant
            For i As Integer = 0 To count - 1
                l.Add(New Windows.Point(CSng(deltaT * (i + 1)), CSng(vc)))
                deltaV = deltaT * (asymptote - vc) / timeConstant
                vc += deltaV
            Next
            Return l
        End Function

#End Region

#Region " TESTS "

        ''' <summary> (Unit Test Method) tests exponent simplex. </summary>
        ''' <remarks>  </remarks>
        <TestMethod()>
        Public Sub ExponentSimplexTest()

            Dim asymptote As Double = PulseResponseSimplexTestInfo.Get.Asymptote
            Dim dataPoints As Integer = PulseResponseSimplexTestInfo.Get.DataPoints
            Dim timeConstant As Double = PulseResponseSimplexTestInfo.Get.TimeConstant

            Dim rnd As Random = Algorithms.Optima.Solution.Random
            Dim expectedAsymptote As Double = asymptote * rnd.NextUniform(0.9, 1.1)
            Dim expectedTimeConstant As Double = timeConstant * rnd.NextUniform(0.9, 1.1)

            ' model initialization parameters
            Dim asymptoteRange As Double() = New Double() {0.5 * asymptote, 2 * asymptote}
            Dim negativeInverseTauRange As Double() = New Double() {-2 / timeConstant, -0.5 / timeConstant}

            Dim noise As List(Of Double) = New List(Of Double)(rnd.Normal(dataPoints, 0, PulseResponseSimplexTestInfo.Get.RelativeNoiseLevel * asymptote))
            Dim noiseSample As New Core.Engineering.SampleStatistics
            noiseSample.AddValues(noise)
            noiseSample.Evaluate()

            Dim timeSeries As List(Of System.Double) = New List(Of System.Double)(PulseResponseSimplexTests.BuildTimeSeries(PulseResponseSimplexTestInfo.Get.SamplingInterval,
                                                                                                                            PulseResponseSimplexTestInfo.Get.DataPoints))
            Dim exponenet As List(Of System.Windows.Point) = New List(Of System.Windows.Point)(PulseResponseSimplexTests.BuildExponent(expectedAsymptote,
                                                                                                                                  expectedTimeConstant, timeSeries))
            Dim testData As List(Of System.Windows.Point) = New List(Of System.Windows.Point)(PulseResponseSimplexTests.Add(exponenet, noise))

            Dim _Model As Algorithms.Optima.PulseResponseFunction = New Algorithms.Optima.PulseResponseFunction(testData) With {
                                                                                    .ObjectiveFunctionMode = PulseResponseSimplexTestInfo.Get.ObjectiveFunctionMode}

            Dim expectedMaximumSSQ As Double = noiseSample.SumSquareDeviations
            Dim objectivePrecision As Double = 0
            Dim asymptoteAccuracy As Double = PulseResponseSimplexTestInfo.Get.AsymptoteAccuracy * expectedAsymptote
            Dim inverseTauAccuracy As Double = PulseResponseSimplexTestInfo.Get.TimeConstantAccuracy / expectedTimeConstant
            Dim testSample As New Core.Engineering.SampleStatistics
            For Each x As System.Windows.Point In testData
                testSample.AddValue(x.Y)
            Next
            Dim ExponentSample As New Core.Engineering.SampleStatistics
            For Each x As System.Windows.Point In exponenet
                ExponentSample.AddValue(x.Y)
            Next
            Dim expectedCorrelationCoeffient As Double = testSample.EvaluateCorrelationCoefficient(ExponentSample.Values)

            If _Model.ObjectiveFunctionMode = Algorithms.Optima.ObjectiveFunctionMode.Deviations Then
                objectivePrecision = PulseResponseSimplexTestInfo.Get.RelativeObjectiveLimit * expectedMaximumSSQ
            ElseIf _Model.ObjectiveFunctionMode = Algorithms.Optima.ObjectiveFunctionMode.Correlation Then
                objectivePrecision = PulseResponseSimplexTestInfo.Get.RelativeObjectiveLimit * (1 - expectedCorrelationCoeffient)
            End If

            Dim simplex As Algorithms.Optima.Simplex
            Dim dimension As Integer = 2
            simplex = New Algorithms.Optima.Simplex("Exponent", dimension,
                                                    New Double() {asymptoteRange(0), negativeInverseTauRange(0)},
                                                    New Double() {asymptoteRange(1), negativeInverseTauRange(1)},
                                                    PulseResponseSimplexTestInfo.Get.IterationCount,
                                                    New Double() {PulseResponseSimplexTestInfo.Get.RelativeConvergenceRadius * asymptoteAccuracy,
                                                                  PulseResponseSimplexTestInfo.Get.RelativeConvergenceRadius * inverseTauAccuracy},
                                                    objectivePrecision)
            simplex.Initialize(Algorithms.Optima.Solution.Random, _Model)
            Dim intialSimplex As String = simplex.ToString
            ' simplex.Solve()
            simplex.Solve(Function()
                              Return simplex.HitCountOverflow OrElse (simplex.Converged AndAlso simplex.Optimized)
                          End Function)

            ' update the model function values
            _Model.EvaluateObjective(simplex.BestSolution.Values)
            Dim actualAsymptote As Double = simplex.BestSolution.Values(0)
            Dim actualTimeConstant As Double = -1 / simplex.BestSolution.Values(1)
            TestInfo.TraceMessage($"Expected  exponent {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})")
            TestInfo.TraceMessage($"Estimated exponent {actualAsymptote:G4}(1-exp(-t/{actualTimeConstant:G4})")
            TestInfo.TraceMessage($"Initial simplex:{Environment.NewLine}{intialSimplex}")
            TestInfo.TraceMessage($" Final simplex:{Environment.NewLine}{simplex}")
            TestInfo.TraceMessage($"Best solution {simplex.BestSolution} found after {simplex.IterationNumber} iterations")
            TestInfo.TraceMessage($"Best solution Objective: {simplex.BestSolution.Objective:G4}; Desired: {objectivePrecision:G4}")
            'TestInfo.TraceMessage("Expected Asymptote = {0:G4}", expectedAsymptote)
            'TestInfo.TraceMessage("Expected Time Constant = {0:G4}", expectedTimeConstant)
            'TestInfo.TraceMessage("Estimated Time Constant = {0:G4}", actualTimeConstant)
            TestInfo.TraceMessage("Correlation Coefficient = {0:G5}", _Model.EvaluateCorrelationCoefficient)
            TestInfo.TraceMessage("Exp.  Corr. Coefficient = {0:G5}", expectedCorrelationCoeffient)
            TestInfo.TraceMessage("         Standard Error = {0:G4}", _Model.EvaluateStandardError(simplex.BestSolution.Objective))
            'TestInfo.TraceMessage("Simulated Noise = {0:G4}", noiseSample.Sigma)
            TestInfo.TraceMessage("Simulated SSQ = {0:G4}", noiseSample.SumSquareDeviations)
            TestInfo.TraceMessage("    Model SSQ = {0:G4}", _Model.EvaluateSquareDeviations)
            TestInfo.TraceMessage($"Converged: {simplex.Converged}")
            TestInfo.TraceMessage($"Optimized: {simplex.Optimized}")

            ' often, the expected solution resides outside the simplex convergence region between the best
            ' and worst simplex nodes. Consequently, the distance between the best and solution and
            ' expected value may exceed the convergence precision. This the expected precision cannot be
            ' used to predict the success of the unit test. 
            Assert.AreEqual(expectedAsymptote, actualAsymptote, PulseResponseSimplexTestInfo.Get.AsymptoteAccuracy * expectedAsymptote,
                        $"Asymptote failed; Converged: {simplex.Converged}; Optimized: {simplex.Optimized}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})")
            Assert.AreEqual(expectedTimeConstant, actualTimeConstant, PulseResponseSimplexTestInfo.Get.TimeConstantAccuracy * expectedTimeConstant,
                        $"Time constant failed; Converged: {simplex.Converged}; Optimized: {simplex.Optimized}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})")

        End Sub

#End Region

    End Class
End Namespace