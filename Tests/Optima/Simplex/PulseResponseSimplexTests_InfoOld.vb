﻿Imports System.Configuration
Namespace OptimaTests

    Partial Public NotInheritable Class Info

#Region " EXPONENET SETTINGS "

        Public Shared ReadOnly Property Asymptote As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property TimeConstant As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property DataPoints As Integer
            Get
                Return My.MyAppSettingsReader.AppSettingInt32()
            End Get
        End Property

        Public Shared ReadOnly Property SamplingInterval As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property


        Public Shared ReadOnly Property RelativeNoiseLevel As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

#End Region

#Region " CONVERGENCE CONDITIONS "

        Public Shared ReadOnly Property ObjectiveFunctionMode As ObjectiveFunctionMode
            Get
                Return My.MyAppSettingsReader.AppSettingEnum(Of ObjectiveFunctionMode)
            End Get
        End Property

        Public Shared ReadOnly Property IterationCount As Integer
            Get
                Return My.MyAppSettingsReader.AppSettingInt32()
            End Get
        End Property

        ''' <summary>
        ''' Gets the relative convergence radius. This is the amount by which the convergence radius is
        ''' compressed to help ensure achieving the convergence accuracy in case the simplex converges
        ''' from outside the expected values.
        ''' </summary>
        ''' <value> The relative convergence radius. </value>
        Public Shared ReadOnly Property RelativeConvergenceRadius As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        ''' <summary> Gets the Asymptote accuracy. </summary>
        ''' <remarks>
        ''' The Asymptote accuracy must be greater than the relative noise level for the unit test to
        ''' pass because the amplitude may change by the noise level.
        ''' </remarks>
        ''' <value> The amplitude accuracy. </value>
        Public Shared ReadOnly Property AsymptoteAccuracy As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property TimeConstantAccuracy As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property RelativeObjectiveLimit As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

#End Region

#Region " RESENBROCK CONDITIONS "

        Public Shared ReadOnly Property RosenbrockSeed As Integer
            Get
                Return My.MyAppSettingsReader.AppSettingInt32()
            End Get
        End Property


        Public Shared ReadOnly Property RosenbrockIterationCount As Integer
            Get
                Return My.MyAppSettingsReader.AppSettingInt32()
            End Get
        End Property

        Public Shared ReadOnly Property RosenbrockObjective As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property RosenbrockConvergence As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property RosenbrockMinimum As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property

        Public Shared ReadOnly Property RosenbrockMaximum As Double
            Get
                Return My.MyAppSettingsReader.AppSettingDouble()
            End Get
        End Property


#End Region

    End Class
End Namespace