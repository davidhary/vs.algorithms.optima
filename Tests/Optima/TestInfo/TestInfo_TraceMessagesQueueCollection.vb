﻿
Partial Friend NotInheritable Class TestInfo

    Partial Public Class TraceMessageQueueCollection
        Inherits ObjectModel.Collection(Of isr.Core.Agnostic.TraceMessagesQueue)
        Public Sub New()
            MyBase.New
            Me.Add(TestInfo.TraceMessagesQueueListener)
            Me.Add(isr.Core.Agnostic.My.MyLibrary.UnpublishedTraceMessages)
        End Sub
    End Class

End Class
