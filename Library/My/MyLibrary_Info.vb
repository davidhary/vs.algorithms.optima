﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ' ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Agnostic.My.ProjectTraceEventId.Optima

        Public Const AssemblyTitle As String = "Optimization Algorithms Library"
        Public Const AssemblyDescription As String = "Optimization Algorithms Library"
        Public Const AssemblyProduct As String = "Algorithms.Optima.2019"

    End Class

End Namespace


