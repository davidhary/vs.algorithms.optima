﻿''' <summary> A Pulse Response function. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="9/23/2017" by="David" revision=""> Created. </history>
Public Class PulseResponseFunction
    Inherits ApproximationFunctionBase

    ''' <summary> Constructor. </summary>
    ''' <param name="observations"> The observations. </param>
    Public Sub New(ByVal observations()() As Double)
        MyBase.New(observations)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="observations"> The observations. </param>
    Public Sub New(ByVal observations As IEnumerable(Of System.Drawing.PointF))
        MyBase.New(observations)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="observations"> The observations. </param>
    Public Sub New(ByVal observations As IEnumerable(Of System.Windows.Point))
        MyBase.New(observations)
    End Sub

    ''' <summary> Gets or sets the objective function mode. </summary>
    ''' <value> The objective function mode. </value>
    Public Property ObjectiveFunctionMode As ObjectiveFunctionMode

    ''' <summary> Evaluates the objective value for the specified arguments. </summary>
    ''' <remarks> Evaluates the sum of squares of differences between <see cref="FunctionValue">the function values</see> and the observations. </remarks>
    ''' <param name="arguments"> The solution values. </param>
    ''' <returns> The objective. </returns>
    Public Overrides Function EvaluateObjective(arguments As IEnumerable(Of Double)) As Double
        Select Case Me.ObjectiveFunctionMode
            Case ObjectiveFunctionMode.Correlation
                Me.EvaluateFunctionValues(arguments)
                Return 1 - Me.EvaluateCorrelationCoefficient
            Case Else
                Me.EvaluateFunctionValues(arguments)
                Return Me.EvaluateSquareDeviations
        End Select
    End Function

    ''' <summary> Function Value. </summary>
    ''' <remarks> Evaluates the pulse response function<para>
    ''' v(t) = V(1-e(-t/T)) where</para><para>
    ''' V = argument(0) and </para><para>
    ''' -1/T = argument(1) </para> </remarks>
    ''' <param name="arguments">    The solution values. </param>
    ''' <param name="observation"> The observation. </param>
    ''' <returns> The function value at the specified arguments. </returns>
    Public Overrides Function FunctionValue(arguments As IEnumerable(Of Double), ByVal observation As IEnumerable(Of Double)) As Double
        If arguments Is Nothing Then Throw New ArgumentNullException(NameOf(arguments))
        If observation Is Nothing Then Throw New ArgumentNullException(NameOf(observation))
        Return arguments(0) * (1 - Math.Exp(observation(0) * arguments(1)))
    End Function

End Class

''' <summary> Values that represent objective function mode. </summary>
Public Enum ObjectiveFunctionMode
    Deviations
    Correlation
End Enum