﻿''' <summary> Rosenbrock objective function. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/20/2014" by="David" revision=""> Created. </history>
Public Class RosenbrockObjectiveFunction
    Inherits ObjectiveFunctionBase

    ''' <summary> Evaluates the value for the specified arguments. </summary>
    ''' <param name="arguments"> The solution values. </param>
    ''' <returns> The objective or nothing if the function failed to evaluate the objective. </returns>
    Public Overrides Function EvaluateObjective(ByVal arguments As IEnumerable(Of Double)) As Double
        If arguments Is Nothing Then Throw New ArgumentNullException(NameOf(arguments))
        Dim x As Double = arguments(0)
        Dim y As Double = arguments(1)
        ' Rosenbrock's function, the function to be minimized
        Return 100.0 * Math.Pow((y - x * x), 2) + Math.Pow(1 - x, 2)
    End Function

End Class
