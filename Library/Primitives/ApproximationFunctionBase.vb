﻿''' <summary> Approximation function base. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/22/2014" by="David" revision=""> Created. </history>
Public MustInherit Class ApproximationFunctionBase
    Inherits ObjectiveFunctionBase

#Region " EVALUATE "

    ''' <summary> Specialized constructor for use only by derived classes. </summary>
    ''' <param name="observations"> The observations. </param>
    Protected Sub New(ByVal observations()() As Double)
        MyBase.New()
        Me._PopulateObservations(observations)
    End Sub

    ''' <summary> Specialized constructor for use only by derived classes. </summary>
    ''' <param name="observations"> The observations. </param>
    Protected Sub New(ByVal observations As IEnumerable(Of System.Drawing.PointF))
        MyBase.New()
        Me._PopulateObservations(observations)
    End Sub

    ''' <summary> Specialized constructor for use only by derived classes. </summary>
    ''' <param name="observations"> The observations. </param>
    Protected Sub New(ByVal observations As IEnumerable(Of System.Windows.Point))
        MyBase.New()
        Me._PopulateObservations(observations)
    End Sub

    ''' <summary> The observations. </summary>
    Private _Observations()() As Double

    ''' <summary> Gets the observations. </summary>
    ''' <returns> The observation values. </returns>
    Protected Function Observations() As Double()()
        Return Me._observations
    End Function

    ''' <summary> Populate observations. </summary>
    ''' <param name="observations"> The observations. </param>
    Private Sub _PopulateObservations(ByVal observations()() As Double)
        If observations Is Nothing Then Throw New ArgumentNullException(NameOf(observations))
        Me._observations = New Double(observations.GetLength(0) - 1)() {}
        For i As Integer = 0 To observations.GetLength(0) - 1
            Dim len As Integer = observations(i).Length
            Me._observations(i) = New Double(len - 1) {}
            Array.Copy(observations(i), Me._observations(i), len)
        Next
    End Sub

    ''' <summary> Initializes this object from the given from collection. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    ''' illegal values. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Private Sub _PopulateObservations(ByVal timeSeries As IEnumerable(Of System.Drawing.PointF))
        If timeSeries Is Nothing Then
            Throw New ArgumentNullException(NameOf(timeSeries))
        ElseIf timeSeries.Count = 0 Then
            Throw New ArgumentException("Time series has no values", NameOf(timeSeries))
        ElseIf timeSeries.Count <= 10 Then
            Throw New ArgumentException("Time series has less than 10 values", NameOf(timeSeries))
        End If
        Me._Observations = New Double(timeSeries.Count - 1)() {}
        For i As Integer = 0 To Observations.GetLength(0) - 1
            Me._Observations(i) = New Double(1) {timeSeries(i).X, timeSeries(i).Y}
        Next
    End Sub

    ''' <summary> Populate observations. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Private Sub _PopulateObservations(ByVal timeSeries As IEnumerable(Of System.Windows.Point))
        If timeSeries Is Nothing Then
            Throw New ArgumentNullException(NameOf(timeSeries))
        ElseIf timeSeries.Count = 0 Then
            Throw New ArgumentException("Time series has no values", NameOf(timeSeries))
        ElseIf timeSeries.Count <= 10 Then
            Throw New ArgumentException("Time series has less than 10 values", NameOf(timeSeries))
        End If
        Me._Observations = New Double(timeSeries.Count - 1)() {}
        For i As Integer = 0 To Observations.GetLength(0) - 1
            Me._Observations(i) = New Double(1) {timeSeries(i).X, timeSeries(i).Y}
        Next
    End Sub

    ''' <summary> Evaluate function values. </summary>
    ''' <param name="arguments"> The arguments. </param>
    Public Sub EvaluateFunctionValues(ByVal arguments As IEnumerable(Of Double))
        If arguments Is Nothing Then Throw New ArgumentNullException(NameOf(arguments))
        Me._Dimension = arguments.Count
        Dim len As Integer = Me.Observations.GetLength(0)
        Dim v As Double() = New Double(len - 1) {}
        For i As Integer = 0 To Me.Observations.GetLength(0) - 1
            v(i) = Me.FunctionValue(arguments, Me.Observations(i))
        Next
        Me._FunctionValues = New List(Of Double)(v)
    End Sub

    ''' <summary> Function Value. </summary>
    ''' <param name="arguments">   The arguments. </param>
    ''' <param name="observation"> The observation. </param>
    ''' <returns> The function value corresponding to the specific observation. </returns>
    Public MustOverride Function FunctionValue(ByVal arguments As IEnumerable(Of Double), ByVal observation As IEnumerable(Of Double)) As Double

    ''' <summary> The function values. </summary>
    Private _FunctionValues As IEnumerable(Of Double)

    ''' <summary> Function Values. </summary>
    ''' <returns> The function values for the last iteration. </returns>
    Public Function FunctionValues() As IEnumerable(Of Double)
        Return Me._FunctionValues
    End Function

    ''' <summary> The dimension=the number of function variables, or arguments. </summary>
    Private _Dimension As Integer

#End Region

#Region " GOODNESS OF FIT "

    ''' <summary> Evaluates average function. </summary>
    ''' <returns> The average. </returns>
    Private Function EvaluateAverageFunction() As Double
        Dim avg As Double = 0
        Dim v As Double
        For Each v In Me._FunctionValues
            avg += v
        Next
        Return avg / Me._FunctionValues.Count
    End Function

    ''' <summary> Evaluates average observation. </summary>
    ''' <returns> The average. </returns>
    Private Function EvaluateAverageObservation() As Double
        Dim avg As Double = 0
        Dim len As Integer = Me.Observations.GetLength(0)
        For i As Integer = 0 To len - 1
            avg += Me.Observations(i)(1)
        Next
        Return avg / len
    End Function

    ''' <summary> Evaluates correlation coefficient. </summary>
    ''' <remarks> Assumes that the function values already exist. </remarks>
    ''' <returns> The correlation coefficient or coefficient of multiple determination. </returns>
    Public Function EvaluateCorrelationCoefficient() As Double
        Dim favg As Double = Me.EvaluateAverageFunction()
        Dim oavg As Double = Me.EvaluateAverageObservation
        Dim fssq As Double = 0
        Dim ossq As Double = 0
        Dim ofssq As Double = 0
        For i As Integer = 0 To Me._FunctionValues.Count - 1
            Dim f As Double = Me._FunctionValues(i) - favg
            Dim o As Double = Me.Observations(i)(1) - oavg
            fssq += f * f
            ossq += o * o
            ofssq += o * f
        Next
        Return ofssq / (Math.Sqrt(fssq) * Math.Sqrt(ossq))
    End Function

    ''' <summary> Evaluates the sum of squares of deviations between observations and function values. </summary>
    ''' <returns> The sum of squares of deviations between observations and function values. </returns>
    Public Function EvaluateSquareDeviations() As Double
        Dim ssq As Double = 0
        For i As Integer = 0 To Me._FunctionValues.Count - 1
            Dim v As Double = Me.Observations(i)(1) - Me._FunctionValues(i)
            ssq += v * v
        Next
        Return ssq
    End Function

    ''' <summary> Evaluates standard error. </summary>
    ''' <param name="objectiveValue"> The objective value == sum of square of deviations. </param>
    ''' <returns> The standard error of the estimate. </returns>
    Public Function EvaluateStandardError(ByVal objectiveValue As Double) As Double
        Return Math.Sqrt(objectiveValue / (Me._FunctionValues.Count - Me._Dimension))
    End Function

#End Region

End Class
