# Optima Project

A library of optimization classes.

## Getting Started

Clone the project along with its requisite projects to their respective relative path. All projects are installed relative to a common path.

### Prerequisites

The following projects are also required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
* [Share](https://www.bitbucket.org/davidhary/vs.share) - Shared snippets
* [Core](https://www.bitbucket.org/davidhary/vs.optima) - Optima Library

```
git clone git@bitbucket.org:davidhary/vs.share.git
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.optima.git
```

### Installing

Install the projects into the following folders:

#### Projects relative path
```
.\Libraries\VS\Share
.\Libraries\VS\Core\Core
.\Libraries\VS\Algorithm\Optima
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](http://www.integratedscientificresources.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md] file at (https://www.bitbucket.org/davidhary/vs.optima/src) for details

## Acknowledgments

* Hat tip to anyone who's code was used
* [Amoeba Method Optimization] (http://msdn.microsoft.com/en-us/magazine/dn201752.aspx) -- Amoeba Method Optimization
* [Its all a remix] (www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow] (https://www.stackoveflow.com)

## Revision Changes

* Version 1.0.5191	03/18/14	Created.
