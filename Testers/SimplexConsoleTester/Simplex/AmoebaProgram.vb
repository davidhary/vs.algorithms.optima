Imports System

' True during development 
#Const Debugging = True

Module AmoebaProgram

    ''' <summary> Amoeba program. </summary>
    ''' <license> (c) 2013 Microsoft Corporation. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <history date="3/19/2014" by="David" revision="1.0.0.0"> Created based on
    ''' Amoeba Method Optimization using C# by James McCaffrey
    ''' http://msdn.microsoft.com/en-us/magazine/dn201752.aspx </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")>
    Friend Class AmoebaProgram
#Const Debugging = True

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Rosenbrock's")>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="args")>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Shared Sub Main(ByVal args() As String)
            Try
                Console.WriteLine(vbCrLf & "Begin amoeba method optimization demo" & vbCrLf)
                Console.WriteLine("Solving Rosenbrock's function f(x,y) = 100*(y-x^2)^2 + (1-x)^2")
                Console.WriteLine("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0" & vbCrLf)

                Dim dimension As Integer = 2 ' problem dimension (number of variables to solve for)
                Dim amoebaSize As Integer = 3 ' number of potential solutions in the amoeba
                Dim minX As Double = -10.0
                Dim maxX As Double = 10.0
                Dim maxLoop As Integer = 50

                Console.WriteLine("Creating amoeba with size = " & amoebaSize)
                Console.WriteLine("Setting max loop = " & maxLoop)
                Dim a As New Amoeba(amoebaSize, dimension, minX, maxX, maxLoop) ' an amoeba method optimization solver

                Console.WriteLine(vbCrLf & "Initial amoeba is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Beginning reflect-expand-contract solve loop" & vbCrLf)
                Dim sln As Solution = a.Solve
                Console.WriteLine(vbCrLf & "Solve complete" & vbCrLf)

                Console.WriteLine("Final amoeba is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Best solution found: " & vbCrLf)
                Console.WriteLine(sln.ToString)

                Console.WriteLine(vbCrLf & "End amoeba method optimization demo" & vbCrLf)
                Console.ReadLine()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Console.ReadLine()
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="dataSource")>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared Function ObjectiveFunction(ByVal vector() As Double, ByVal dataSource As Object) As Double
            ' Rosenbrock's function, the function to be minimized
            ' no data source needed here but real optimization problems will often be based on data
            Dim x As Double = vector(0)
            Dim y As Double = vector(1)
            Return 100.0 * Math.Pow((y - x * x), 2) + Math.Pow(1 - x, 2)
            'Return (x * x) + (y * y) 'sphere function
        End Function

    End Class ' program class

    ''' <summary> A potential Solution. </summary>
    ''' <license> (c) 2013 Microsoft Corporation. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <history date="3/19/2014" by="David" revision="1.0.0.0"> Documented. </history>
    Public Class Solution
        Implements IComparable(Of Solution)

        ' a potential solution (array of double) and associated value (so can be sorted against several potential solutions
        Public vector() As Double
        Public value As Double

#Const Debugging = True
#If Debugging Then
        Shared random As New System.Random(1) ' DateTime.Now.Millisecond)
#Else
    Shared random As New System.Random(DateTime.Now.Millisecond)
#End If

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Sub New(ByVal dimension As Integer, ByVal minX As Double, ByVal maxX As Double)
            ' a random Solution
            Me.vector = New Double(dimension - 1) {}
            For i As Integer = 0 To dimension - 1
                Me.vector(i) = (maxX - minX) * random.NextDouble + minX
            Next i
            Me.value = AmoebaProgram.ObjectiveFunction(Me.vector, Nothing)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Sub New(ByVal vector() As Double)
            ' a specified solution
            Me.vector = New Double(vector.Length - 1) {}
            Array.Copy(vector, Me.vector, vector.Length)
            Me.value = AmoebaProgram.ObjectiveFunction(Me.vector, Nothing)
        End Sub

        Public Function CompareTo(ByVal other As Solution) As Integer Implements IComparable(Of Solution).CompareTo ' based on vector/solution value
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            If Me.value < other.value Then
                Return -1
            ElseIf Me.value > other.value Then
                Return 1
            Else
                Return 0
            End If
        End Function

        Public Overrides Function ToString() As String
            Dim s As String = "[ "
            For i As Integer = 0 To Me.vector.Length - 1
                If vector(i) >= 0.0 Then
                    s &= " "
                End If
                s &= vector(i).ToString("F2") & " "
            Next i
            s &= "] = " & Me.value.ToString("F4")
            Return s
        End Function
    End Class

    ''' <summary> Amoeba method numerical optimization. </summary>
    ''' <license> (c) 2013 Microsoft Corporation. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <remarks> Reference:<para>A Simplex Method for Function Minimization, J.A. Nelder and R. Mead,
    ''' The Computer Journal, vol. 7, no. 4, 1965, pp.308-313.</para> </remarks>
    ''' <history date="3/19/2014" by="David" revision="1.0.0.0"> Documented. </history>
    Public Class Amoeba
        Public amoebaSize As Integer ' number of solutions
        Public dimension As Integer ' vector-solution size, also problem dimension
        Public solutions() As Solution ' potential solutions (vector + value)

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")>
        Public minX As Double
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")>
        Public maxX As Double

        Public alpha As Double ' reflection
        Public beta As Double ' contraction
        Public gamma As Double ' expansion

        Public maxLoop As Integer ' limits main solving loop

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Sub New(ByVal amoebaSize As Integer, ByVal dimension As Integer, ByVal minX As Double, ByVal maxX As Double, ByVal maxLoop As Integer)
            Me.amoebaSize = amoebaSize
            Me.dimension = dimension
            Me.minX = minX
            Me.maxX = maxX
            Me.alpha = 1.0 ' hard-coded values from theory
            Me.beta = 0.5
            Me.gamma = 2.0

            Me.maxLoop = maxLoop

            Me.solutions = New Solution(amoebaSize - 1) {}
            For i As Integer = 0 To solutions.Length - 1
                solutions(i) = New Solution(dimension, minX, maxX) ' the Solution ctor calls the objective function to compute value
            Next i

            Array.Sort(solutions)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function Centroid() As Solution
            ' return the centroid of all solution vectors except for the worst (highest index) vector
            Dim c(dimension - 1) As Double
            For i As Integer = 0 To amoebaSize - 2
                For j As Integer = 0 To dimension - 1
                    c(j) += solutions(i).vector(j) ' accumulate sum of each vector component
                Next j
            Next i

            For j As Integer = 0 To dimension - 1
                c(j) = c(j) / (amoebaSize - 1)
            Next j

            Dim s As New Solution(c) ' feed vector to ctor which calls objective function to compute value
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function Reflected(ByVal centroid As Solution) As Solution
            ' the reflected solution extends from the worst (lowest index) solution through the centroid
            Dim r(dimension - 1) As Double
            Dim worst() As Double = Me.solutions(amoebaSize - 1).vector ' convenience only
            For j As Integer = 0 To dimension - 1
                r(j) = ((1 + alpha) * centroid.vector(j)) - (alpha * worst(j))
            Next j
            Dim s As New Solution(r)
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function Expanded(ByVal reflected As Solution, ByVal centroid As Solution) As Solution
            ' expanded extends even more, from centroid, thru reflected
            Dim e(dimension - 1) As Double
            For j As Integer = 0 To dimension - 1
                e(j) = (gamma * reflected.vector(j)) + ((1 - gamma) * centroid.vector(j))
            Next j
            Dim s As New Solution(e)
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function Contracted(ByVal centroid As Solution) As Solution
            ' contracted extends from worst (lowest index) towards centoid, but not past centroid
            Dim v(dimension - 1) As Double ' didn't want to reuse 'c' from centoid routine
            Dim worst() As Double = Me.solutions(amoebaSize - 1).vector ' convenience only
            For j As Integer = 0 To dimension - 1
                v(j) = (beta * worst(j)) + ((1 - beta) * centroid.vector(j))
            Next j
            Dim s As New Solution(v)
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Sub Shrink()
            ' move all vectors, except for the best vector (at index 0), halfway to the best vector
            ' compute new objective function values and sort result
            For i As Integer = 1 To amoebaSize - 1 ' note we don't start at 0
                For j As Integer = 0 To dimension - 1
                    solutions(i).vector(j) = (solutions(i).vector(j) + solutions(0).vector(j)) / 2.0
                    solutions(i).value = AmoebaProgram.ObjectiveFunction(solutions(i).vector, Nothing)
                Next j
            Next i
            Array.Sort(solutions)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Sub ReplaceWorst(ByVal newSolution As Solution)
            ' replace the worst solution (at index size-1) with contents of parameter newSolution's vector
            For j As Integer = 0 To dimension - 1
                solutions(amoebaSize - 1).vector(j) = newSolution.vector(j)
            Next j
            solutions(amoebaSize - 1).value = newSolution.value
            Array.Sort(solutions)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function IsWorseThanAllButWorst(ByVal reflected As Solution) As Boolean
            ' Solve needs to know if the reflected vector is worse (greater value) than every vector in the amoeba, except for the worst vector (highest index)
            For i As Integer = 0 To amoebaSize - 2 ' not the highest index (worst)
                If reflected.value <= solutions(i).value Then ' reflected is better (smaller value) than at least one of the non-worst solution vectors
                    Return False
                End If
            Next i
            Return True
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function Solve() As Solution
            Dim t As Integer = 0 ' loop counter
            Do While t < maxLoop
                t += 1

#If Debugging Then
                Console.Write("At t = " & t & " current best solution = ")
                Console.WriteLine(Me.solutions(0))
#Else
                If t = 1 OrElse t Mod 10 = 0 Then
                    Console.Write("At t = " & t & " current best solution = ")
                    Console.WriteLine(Me.solutions(0))
                End If
#End If

                Dim centroid As Solution = Me.Centroid ' compute centroid
                Dim reflected As Solution = Me.Reflected(centroid) ' compute reflected

                If reflected.value < solutions(0).value Then ' reflected is better than the current best
                    Dim expanded As Solution = Me.Expanded(reflected, centroid) ' can we do even better??
                    If expanded.value < solutions(0).value Then ' winner! expanded is better than current best
                        ReplaceWorst(expanded) ' replace current worst solution with expanded
                    Else
                        ReplaceWorst(reflected) ' it was worth a try...
                    End If
                    Continue Do
                End If

                If IsWorseThanAllButWorst(reflected) = True Then ' reflected is worse (larger value) than all solution vectors (except possibly the worst one)
                    If reflected.value <= solutions(amoebaSize - 1).value Then ' reflected is better (smaller) than the curr worst (last index) vector
                        ReplaceWorst(reflected)
                    End If

                    Dim contracted As Solution = Me.Contracted(centroid) ' compute a point 'inside' the amoeba

                    If contracted.value > solutions(amoebaSize - 1).value Then ' contracted is worse (larger value) than curr worst (last index) solution vector
                        Shrink()
                    Else
                        ReplaceWorst(contracted)
                    End If

                    Continue Do
                End If

                ReplaceWorst(reflected)

                'If IsSorted = false Then
                '  throw New Exception("Unsorted at k = " &k)
                'End If
            Loop ' solve loop

            Return solutions(0) ' best solution is always at 0
        End Function

#If Debugging Then
        ''' <summary> Checks if sorted. </summary>
        ''' <returns> <c>True</c> if sorted. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function IsSorted() As Boolean
            For i As Integer = 0 To solutions.Length - 2
                If solutions(i).value > solutions(i + 1).value Then
                    Return False
                End If
            Next i
            Return True
        End Function
#End If

        Public Overrides Function ToString() As String
            Dim s As String = ""
            For i As Integer = 0 To solutions.Length - 1
                s &= "[" & i & "] " & solutions(i).ToString & Environment.NewLine
            Next i
            Return s
        End Function

    End Class ' class Amoeba

End Module

