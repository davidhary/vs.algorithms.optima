Imports System

' True during development 
#Const Debugging = True

Module SimplexProgram

    ''' <summary> Simplex program. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <history date="3/19/2014" by="David" revision="1.0.0.0"> Created based on
    ''' Amoeba Method Optimization using C# by James McCaffrey
    ''' http://msdn.microsoft.com/en-us/magazine/dn201752.aspx </history>
    Friend Class SimplexProgram

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Rosenbrock's")>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="args")>
        Shared Sub Main(ByVal args() As String)
            Try
                Console.WriteLine(vbCrLf & "Begin simplex method optimization demo" & vbCrLf)
                Console.WriteLine("Solving Rosenbrock's function f(x,y) = 100*(y-x^2)^2 + (1-x)^2")
                Console.WriteLine("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0" & vbCrLf)

                Dim dimension As Integer = 2 ' problem dimension (number of variables to solve for)
                Dim simplexSize As Integer = dimension + 1 ' number of potential solutions in the simplex
                Dim minX As Double = -10.0
                Dim maxX As Double = 10.0
                Dim maxLoop As Integer = 50
                Dim objectivePrecision As Double = 0.01
                Dim valuesPrecision As Double = 0.01

                Console.WriteLine("Creating simplex with size = " & simplexSize)
                Console.WriteLine("Setting max loop = " & maxLoop)
                ' an simplex method optimization solver
                Dim a As New Simplex("Rosenbrock", dimension, minX, maxX, maxLoop, valuesPrecision, objectivePrecision)
                a.Initialize(New RosenbrockObjectiveFunction)

                Console.WriteLine(vbCrLf & "Initial simplex is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Beginning reflect-expand-contract solve loop" & vbCrLf)
                Dim sln As Solution = a.Solve
                Console.WriteLine(vbCrLf & "Solve complete" & vbCrLf)

                Console.WriteLine("Final simplex is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Best solution found after " & a.IterationNumber & " iterations: " & vbCrLf)
                Console.WriteLine(sln.ToString)

                Console.WriteLine(vbCrLf & "End simplex method optimization demo" & vbCrLf)
                Console.ReadLine()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Console.ReadLine()
            End Try
        End Sub

    End Class ' program class

End Module

