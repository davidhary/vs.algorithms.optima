﻿Imports System.Runtime.CompilerServices

''' <summary> Perform a ternary search. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Ternary
    Inherits SolverBase

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <param name="minimumValue">       The minimum value. </param>
    ''' <param name="maximumValue">       The maximum value. </param>
    ''' <param name="maximumIterations">  The maximum iterations. </param>
    ''' <param name="valuesPrecision">    The values precision. </param>
    ''' <param name="objectivePrecision"> The objective precision. </param>
    Public Sub New(ByVal minimumValue As Double, ByVal maximumValue As Double,
                   ByVal maximumIterations As Integer, ByVal valuesPrecision As Double,
                   ByVal objectivePrecision As Double)
        MyBase.New("ternary", 2, 100, New Double(1) {valuesPrecision, valuesPrecision}, objectivePrecision)
        Me._minimumValue = minimumValue
        Me._maximumValue = maximumValue
        MyBase.ConvergenceRadius = valuesPrecision
        MyBase.ObjectiveLimit = objectivePrecision
        MyBase.MaximumIterations = maximumIterations
    End Sub

    ''' <summary> The minimum value. </summary>
    Private _MinimumValue As Double

    ''' <summary> The maximum value. </summary>
    Private _MaximumValue As Double

    ''' <summary> The values prevision. </summary>
    Private _ValuesPrecisions() As Double

    ''' <summary> The values precisions. </summary>
    ''' <returns> The values precisions. </returns>
    Public Overrides ReadOnly Property ConvergenceRadii() As IEnumerable(Of Double)
        Get
            Return Me._ValuesPrecisions
        End Get
    End Property

#End Region

#Region " INITIALIZE "

    ''' <summary> Initializes the simplex with a random set of solution values. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    ''' null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub Initialize(ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then
            Throw New ArgumentNullException("objectiveFunction")
        End If
        Me.ObjectiveFunction = objectiveFunction
        Me.Solutions = New Solution(1) {Me.CreateSolution(New Double(0) {Me._MinimumValue}, objectiveFunction),
                                      Me.CreateSolution(New Double(0) {Me._MaximumValue}, objectiveFunction)}
        ' 0 is the left solution; 1 is the right solution
    End Sub


#End Region

#Region " TERNARY "

    ''' <summary> Gets the left. </summary>
    ''' <returns> The left side solution. </returns>
    Private Function Left() As Solution
        Return Me.Solutions(0)
    End Function

    ''' <summary> Gets the right. </summary>
    ''' <returns> The right side solution. </returns>
    Private Function Right() As Solution
        Return Me.Solutions(1)
    End Function

    Public Overrides ReadOnly Property BestSolution As Solution
        Get
            Return Me.Centroid
        End Get
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
    Private Function CreateSolution(ByVal values As IEnumerable(Of Double), ByVal objectiveFunction As ObjectiveFunctionBase) As Solution
        Return New Solution(values, ObjectiveFunctionBase.ValidateObjectiveFunction(objectiveFunction).EvaluateObjective(values))
    End Function


    ''' <summary> Calculates the centroid solution. </summary>
    ''' <returns> The centroid of all solution vectors except for the worst (highest index) vector. </returns>
    Protected Overrides Function Centroid() As Solution
        If Left.Equals(Right) Then
            Return New Solution(Left)
        Else
            Dim argument As Double = Left.Values(0) - Left.Objective * (Right.Values(0) - Left.Values(0)) / (Right.Objective - Left.Objective)
            Return CreateSolution(New Double(0) {argument}, Me.ObjectiveFunction)
        End If
    End Function

#End Region

#Region " SOLVE "

    ''' <summary> Move left. </summary>
    ''' <param name="stepSize"> Size of the step. </param>
    Private Sub MoveLeft(ByVal stepSize As Double)
        Me.Right.Objective = Me.Left.Objective
        Me.Right.CopyFrom(New Double(0) {Me.Left.Values(0)})
        Me.Left.CopyFrom(New Double(0) {Me.Left.Values(0) - stepSize})
        Me.Left.Objective = Me.ObjectiveFunction.EvaluateObjective(Me.Left.Values)
    End Sub

    ''' <summary> Move right. </summary>
    ''' <param name="stepSize"> Size of the step. </param>
    Private Sub MoveRight(ByVal stepSize As Double)
        Me.Left.Objective = Me.Right.Objective
        Me.Left.CopyFrom(New Double(0) {Me.Right.Values(0)})
        Me.Right.CopyFrom(New Double(0) {Me.Right.Values(0) + stepSize})
        Me.Right.Objective = Me.ObjectiveFunction.EvaluateObjective(Me.Right.Values)
    End Sub

    ''' <summary> Expands. </summary>
    ''' <param name="stepSize"> Size of the step. </param>
    Private Sub Expand(ByVal stepSize As Double)
        Me.Left.CopyFrom(New Double(0) {Me.Left.Values(0) - stepSize})
        Me.Right.CopyFrom(New Double(0) {Me.Right.Values(0) + stepSize})
        Me.Left.Objective = Me.ObjectiveFunction.EvaluateObjective(Me.Left.Values)
        Me.Right.Objective = Me.ObjectiveFunction.EvaluateObjective(Me.Right.Values)
    End Sub

    ''' <summary> Step towards root. </summary>
    ''' <param name="stepSize"> Size of the step. </param>
    Private Sub StepTowardsRoot(ByVal stepSize As Double)
        ' left and right are the current bounds; the location of the root is outside the range
        If Me.Left.Objective.CompareTo(0) > 0 Then
            ' value is above the function
            Dim slope As Integer = 0
            Select Case slope
                Case 1
                    ' with positive slope, move left
                    Me.MoveLeft(stepSize)
                Case 0
                    ' with unknown slope, move based on the closer value.
                    Dim leftCompareRight As Integer = Me.Left.Objective.CompareTo(Me.Right.Objective)
                    Select Case leftCompareRight
                        Case 1
                            ' if left is larger, move right 
                            Me.MoveRight(stepSize)
                        Case 0
                            ' if left and right have the same value, expand the range.
                            Me.Expand(stepSize)
                        Case -1
                            ' if right is larger, move left
                            Me.MoveLeft(stepSize)
                    End Select
                Case -1
                    ' with negative slope, move right
                    Me.MoveLeft(stepSize)
            End Select
        Else
            ' value is below the function
            Dim slope As Integer = 0
            Select Case slope
                Case 1
                    ' if slope is positive, move right
                    Me.MoveRight(stepSize)
                Case 0
                    ' is slope is unknown, use the closer value
                    Dim leftCompareRight As Integer = Me.Left.Objective.CompareTo(Me.Right.Objective)
                    Select Case leftCompareRight
                        Case 1
                            ' left is larger, move left
                            Me.MoveLeft(stepSize)
                        Case 0
                            ' if left and right have the same value, expand the range.
                            Me.Expand(stepSize)
                        Case -1
                            ' right is larger, move right
                            Me.MoveRight(stepSize)
                    End Select
                Case -1
                    ' if slope is negative, move left
                    Me.MoveLeft(stepSize)
            End Select
        End If
    End Sub

    ''' <summary> Contracts. </summary>
    ''' <param name="stepSize"> Size of the step. </param>
    Private Sub Contract(ByVal stepSize As Double)
        ' left and right are the current bounds; the solution is between them
        Dim leftThird As Solution = Me.CreateSolution(New Double(0) {Me.Left.Values(0) + stepSize / 3}, Me.ObjectiveFunction)

        If leftThird.Objective.Approximates(Me.ObjectiveLimit) Then
            ' left has it; this will terminate on the next iteration.
            Me.Solutions = New Solution(1) {Me.Solutions(0), leftThird}
        ElseIf Me.Left.Objective.CompareTo(0) <> leftThird.Objective.CompareTo(0) Then
            ' value between left and left third
            ' left has it
            Me.Solutions = New Solution(1) {Me.Solutions(0), leftThird}
        Else

            Dim rightThird As Solution = Me.CreateSolution(New Double(0) {Me.Right.Values(0) - stepSize / 3}, Me.ObjectiveFunction)

            If rightThird.Objective.Approximates(Me.ObjectiveLimit) Then
                ' right has it; this will terminate on the next iteration.
                Me.Solutions = New Solution(1) {rightThird, Me.Solutions(1)}
            ElseIf leftThird.Objective.CompareTo(0) <> rightThird.Objective.CompareTo(0) Then
                ' value is between left and right thirds.
                Me.Solutions = New Solution(1) {leftThird, rightThird}
            ElseIf rightThird.Objective.CompareTo(0) <> Me.Right.Objective.CompareTo(0) Then
                ' value is between right and right third.
                Me.Solutions = New Solution(1) {rightThird, Me.Solutions(1)}
            Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled case", "Unhandled case 4 tuples ({0},{1}) ({2},{3}) ({4},{5}) ({6},{7})",
                             Me.Left.Values(0), Me.Left.Objective,
                             leftThird.Values(0), leftThird.Objective,
                             rightThird.Values(0), rightThird.Objective,
                             Me.Right.Values(0), Me.Right.Objective)
            End If
        End If
    End Sub

    ''' <summary> Solves one step. </summary>
    Public Overrides Sub SolveStep()

        Dim stepSize As Double = Me.Right.Values(0) - Me.Left.Values(0)
        If Me.Left.Objective.CompareTo(0) = Me.Right.Objective.CompareTo(0) Then
            ' left and right are the current bounds; the location of the root is outside the range
            Me.StepTowardsRoot(stepSize)
        Else
            Me.Contract(stepSize)
        End If

    End Sub

#End Region

End Class

Public Module Extensions

    ''' <summary>
    ''' Returns True if the value approximates the reference within some delta.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="reference">The reference.</param>
    ''' <param name="delta">The delta.</param>
    <Extension()>
    Public Function Approximates(ByVal value As Double, ByVal reference As Double, ByVal delta As Double) As Boolean
        Return Math.Abs(value - reference) <= delta
    End Function

    ''' <summary>
    ''' Returns True if the value approximates zero within some delta.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="delta">The delta.</param>
    <Extension()>
    Public Function Approximates(ByVal value As Double, ByVal delta As Double) As Boolean
        Return value.Approximates(0, delta)
    End Function

End Module